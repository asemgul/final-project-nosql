<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/main', function () {
    return view('main');
});


Route::middleware(['auth:sanctum', 'verified'])->get('/main', function () {
    return view('main');
})->name('main');

Route::get('/welcome',[\App\Http\Controllers\VacancyController::class,'index'])->name('vacancy.index');
Route::get('create',[\App\Http\Controllers\VacancyController::class,'create'])->name('vacancy.create');
Route::post('store',[\App\Http\Controllers\VacancyController::class,'store'])->name('vacancy.store');
Route::get('edit/{id}',[\App\Http\Controllers\VacancyController::class,'edit'])->name('vacancy.edit');
Route::post('update',[\App\Http\Controllers\VacancyController::class,'update'])->name('vacancy.update');
Route::post('destroy/{id}',[\App\Http\Controllers\VacancyController::class,'destroy'])->name('vacancy.destroy');
//Route::resource('vacancy', \App\Http\Controllers\VacancyController::class);
//Route::resource('items', ItemController::class,['names'=>'items']);

Route::get('/filterPage',[\App\Http\Controllers\VacancyController::class,'filterPage'])->name('vacancy.filterPage');
Route::post('/searchByCity',[\App\Http\Controllers\VacancyController::class,'searchByCity'])->name('vacancy.searchByCity');
Route::get('/sortAscending',[\App\Http\Controllers\VacancyController::class,'sortAscending'])->name('vacancy.sortAscending');
Route::get('/sortDescending',[\App\Http\Controllers\VacancyController::class,'sortDescending'])->name('vacancy.sortDescending');


//Route::redirect('items', '/');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
