@extends('templates.template')
<body style="background-color: #1a202c">

@section('content')
    @if (auth()->check())
        <h1> Hello User! </h1>
        <h1 style="color: white">This is the best online platform to upskill!</h1>
{{--        @if (auth()->user()->isAdmin())--}}
{{--            <h1 style="text-align: center; color: white">Hello, Admin</h1>--}}
{{--            <h1 style="text-align: center; color: white"></h1>--}}
{{--        @elseif(auth()->user()->isCoordinator())--}}
{{--            <h1 style="text-align: center;color: white">Hello, Coordinator</h1>--}}
{{--        @elseif(auth()->user()->isAuthor())--}}
{{--            <h1 style="text-align: center;color: white">Hello, Author</h1>--}}
{{--        @elseif(auth()->user()->isUser())--}}
{{--            <div style="text-align: center">--}}
{{--                <h1 style="color: white">Hello, User</h1>--}}
{{--                <h1 style="color: white">This is the best online platform to upskill!</h1>--}}
{{--                <a href="{{url('/problems')}}" class="btn btn-light btn-lg"  style="margin-top: 20px;width: 200px;">Let's Code!</a>--}}
{{--            </div>--}}
{{--        @endif--}}
    @else
        <div style="text-align: center">
            <h1 style="color: white">Fill free while using this platform</h1>
            <img src="image/help.png" alt="Italian Trulli">
{{--            <a href="{{route('login')}}" class="btn btn-light btn-lg"  style="margin-top: 20px;width: 200px;">Login</a>--}}
        </div>
    @endif
</body>


@endsection
